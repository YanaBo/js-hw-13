$(document).ready(function () {
  const $tabs = $('.tabs');
  const $tabTitle = $('.tabs-title');
  const $tabText = $('.tabs-text');

  $tabs.on('click', changingTab);

  $tabTitle.first().addClass('active');
  $tabText.first().css({'display': 'block'});

  function changingTab(event) {
    const $target = $(event.target);
    const dataTab = $target.data('tab');
    const targetClass = $target.attr('class');

    if (targetClass === 'tabs-title') {
      for (let i = 0; i < $tabTitle.length; i++) {
        $tabTitle.eq(i).removeClass('active');
      }

      $target.addClass('active');

      for (let i = 0; i < $tabText.length; i++) {
        if (dataTab === i) {
          $tabText.eq(i).css({'display': 'block'});
        } else {
          $tabText.eq(i).css({'display': 'none'});
        }
      }
    }
  }
});